import LsLoading from './ls-loading.js'

export default class DiscogsLabel extends HTMLElement {
	// the html custom-component properties we're watching
	static get observedAttributes() {
		return ['label-id']
	}

	discogsApiUrl = 'https://api.discogs.com/labels/'

	get state() {
		return this._state || null
	}

	set state(data) {
		this._state = data || {}
		this.render()
	}

	constructor() {
		super()

		let labelId = this.getAttribute('label-id')
		if (!labelId) return

		this.fetchData(labelId).then(data => {
			this.state = data
		})

	}

	async fetchData(labelId) {
		const labelUrl = this.discogsApiUrl + labelId
		const labelInfo = await fetch(labelUrl).then(data => data.json())
		return labelInfo
	}

	attributeChangedCallback() {
		this.render()
	}

	render() {
		if (!this.state) return this.innerHTML = `<ls-loading></ls-loading>`

		const {name, uri, profile, urls} = this.state

		this.innerHTML = `
			<main>
				<h1>${ urls.length ? `<a href="${urls[0]}">${name}</a>` : name }</h1>
		<p>${profile}</p>
		<nav>
				<a href="${uri}">Discogs</a> ${ urls.length && urls.map(u => `<a href="${u}">${u}</a>`).join('') }
		</nav>
			</main>
				`
	}
}

customElements.define('discogs-label', DiscogsLabel)
