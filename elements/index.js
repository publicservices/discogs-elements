import DiscogsLabel from './discogs-label.js'
import DiscogsLabelReleases from './discogs-label-releases.js'
import DiscogsRelease from './discogs-release.js'

export default {
	DiscogsLabel,
	DiscogsLabelReleases,
	DiscogsRelease
}
