export default class DiscogsPlayRelease extends HTMLButtonElement {
	discogsApiUrl = releaseId => `https://api.discogs.com/releases/${releaseId}`

	connectedCallback() {
		let releaseId = this.getAttribute('release-id')
		if (!releaseId) return
		this.fetchData(releaseId).then(data => {
			this.state = data
		}).then(() => {
			this.addEventListener('click', this.handleClick)
		})
	}

	disconnectedCallback() {
		this.removeEventListener('click', this.handleClick)
	}

	async fetchData(releaseId) {
		const releaseUrl = this.discogsApiUrl(releaseId)
		const data = await fetch(releaseUrl).then(res => res.json())
		return data
	}

	preparePlaylist(release) {
		const { title, artists_sort, thumb, videos } = release

		const tracks = videos.map(video => {
			return {
				id: btoa(video.uri),
				title: video.title,
				url: video.uri
			}
		}).reverse()

		return {
			title: `${artists_sort} - ${title}`,
			image: thumb,
			tracks
		}
	}

	handleClick() {
		const playlist = this.preparePlaylist(this.state)
		this.loadPlaylist(playlist)
	}
	loadPlaylist(playlist) {
		// Get access to the Vue component behind the web component to access methods,
		// and update player with our new playlist.
		var player = document.querySelector('radio4000-player')

		if (!player) return

		var vue = player.getVueInstance()
		vue.updatePlaylist(playlist)
	}
}

customElements.define('discogs-play-release', DiscogsPlayRelease, { extends: 'button' })
