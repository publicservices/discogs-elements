import LsLoading from './ls-loading.js'
import DiscogsPlayRelease from './discogs-play-release.js'

export default class DiscogsRelease extends HTMLElement {
	// the html custom-component properties we're watching
	static get observedAttributes() {
		return ['release-id']
	}

	discogsApiUrl = releaseId => `https://api.discogs.com/releases/${releaseId}`

	get state() {
		return this._state || null
	}

	set state(data) {
		this._state = data || {}
		this.render()
	}

	constructor() {
		super()

		let releaseId = this.getAttribute('release-id')
		this.fetchData(releaseId).then(data => {
			this.state = data
		})

	}

	async fetchData(releaseId) {
		let headers = new Headers({
			"User-Agent" : "npm/discogs-elements"
		})
		const releaseUrl = this.discogsApiUrl(releaseId)
		const info = await fetch(releaseUrl, {
			method: 'GET',
			headers
		}).then(data => data.json())
		return info
	}

	attributeChangedCallback() {
		this.render()
	}

	render() {
		if (!this.state) return this.innerHTML = `<ls-loading></ls-loading>`

		const { artists_sort, title, uri, videos } = this.state
		const { catno } = this.state.labels[0]

		const releaseId = this.getAttribute('release-id')

		this.innerHTML = `
			<main>
				<a href="${uri}">
					[${catno}] <strong>${artists_sort}</strong>: ${title}
				</a>
				${ videos.length && `<button is="discogs-play-release" release-id="${releaseId}">Play</button>` }
		</main>
		`
	}
}

customElements.define('discogs-release', DiscogsRelease)
