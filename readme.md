> Work in progress

# discogs-elements

This is an attempt at creating discogs HTML web components in vanilla
javascript. It is using the latest browser features and should work
nativelly without the need of polyfill in modern browsers.

You can use them like this in any HTML document.

```
<discogs-label label-id="6785"></discogs-label>
<discogs-label-releases label-id="6785"></discogs-label-releases>
```

For them to work, you need to import the bit of javascript code in
which these elements and their behavior are defined.

You can import the script from a CDN or from the npm package if
your are in a npm project.

```
<script type="module" async src="discogs-elements"></script>
```

or:

```
<script type="module" async src="https://cdn.jsdelivr.net/npm/discogs-elements@latest"></script>
```

Do not forget the `type="module"` on the script tag, otherwise you'll
get an error `Uncaught SyntaxError: Cannot use import statement
outside a module`.

## Examples

Examples can be found in the files `index.html` and `test.html`.
